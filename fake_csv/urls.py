from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from core import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.MainView.as_view()),

    path('signup/', views.SignUpView.as_view()),
    path('login/', LoginView.as_view(template_name="login.html")),
    path('logout/', LogoutView.as_view()),

    path('create/', views.CreateView.as_view(), name='create'),
    path('delete/', views.DeleteColumnView.as_view(), name='delete_schemas_column'),
    path('submit/', views.SubmitCreateSchemaView.as_view(), name='submit_create_schema'),
    path('download/<int:id>/', views.DownloadPageView.as_view(), name='download_page'),
    path('download/<path:file_path>/', views.DownloadCSVView.as_view(), name='download_csv'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

