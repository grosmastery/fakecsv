from django.db import models
from django.contrib.auth import get_user_model


class ObjectsModel(models.Model):
    objects = models.Manager()


class SchemaModel(ObjectsModel):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    schema_name = models.CharField(max_length=100)
    modified = models.DateTimeField(auto_now=True)
    columns = models.ManyToManyField('TableColumnModel')
    closed = models.BooleanField(default=False)
    csv_file = models.FileField(blank=True, null=True)
    csv_delimiter = models.CharField(max_length=2, default=',')
    csv_quotechar = models.CharField(max_length=2, default='"')

    def __str__(self):
        return self.schema_name


class TableColumnModel(ObjectsModel):
    column_name = models.CharField(max_length=100)
    type_field = models.CharField(max_length=100)
    integer_min = models.CharField(max_length=100, blank=True, null=True)
    integer_max = models.CharField(max_length=100, blank=True, null=True)
    order = models.CharField(max_length=100)

    def __str__(self):
        return self.column_name
