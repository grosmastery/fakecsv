$(document).ready(function() {

    $('#generate-data').click(function(e) {
        e.preventDefault();
        let rows = $('#rows').val();
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#div-generate-data').attr("action");
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'rows': rows,
                csrfmiddlewaretoken: token
            },
            success: function(data) {
                console.log(data)
                window.location.reload(true)

            },
            errors: function() {
                console.log("error")
            }
        });
    });
});
