from django import forms
from django.contrib.auth import get_user_model


class SignUpForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100,
                               widget=forms.TextInput(attrs={'placeholder': 'Enter username'}))
    email = forms.EmailField(label='Email', max_length=100,
                             widget=forms.EmailInput(attrs={'placeholder': 'Enter email'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'placeholder': 'Enter password'}))
    confirm_password = forms.CharField(label='Confirm Password',
                                       widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}))

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')

        if password != confirm_password:
            raise forms.ValidationError('Password and Confirm password must be the same')

        return cleaned_data

    def save(self, commit=True):
        user = get_user_model().objects.create_user(self.cleaned_data['username'], self.cleaned_data['email'],
                                                    self.cleaned_data['password'])
        return user


class CreateSchemaForm(forms.Form):
    TYPES_OF_DATA = (
        ('full_name', 'Full Name'),
        ('job', 'Job'),
        ('email', 'Email'),
        ('domain_name', 'Domain Name'),
        ('phone_number', 'Phone Number'),
        ('company_name', 'Company Name'),
        ('text', 'Text'),
        ('integer', 'Integer'),
        ('address', 'Address'),
    )

    DELIMITERS = (
        (',', 'Comma ( , )'),
        ('.', 'Dot ( . )'),
        (';', 'Semicolon ( ; )'),
        ('/', 'Slash ( / )'),
        ('|', 'Pipe ( | )'),
        ('-', 'Dash ( - )'),
        ('_', 'Underscore ( _ )'),
        ('~', 'Tilde ( ~ )'),
        ('#', 'Hash ( # )'),
    )

    QUOTECHARS = (
        ('"', 'Double Quote ( " )'),
        ("'", "Single Quote ( ' )"),
        ('`', 'Backtick ( ` )'),
    )

    schema_name = forms.CharField(max_length=100, required=True)
    data_type = forms.ChoiceField(choices=TYPES_OF_DATA, required=True)
    delimiter = forms.ChoiceField(choices=DELIMITERS, required=True)
    quotechar = forms.ChoiceField(choices=QUOTECHARS, required=True)

    schema_name.widget.attrs.update({'class': 'form-control'})
    data_type.widget.attrs.update({'class': 'form-select'})
    delimiter.widget.attrs.update({'class': 'form-select'})
    quotechar.widget.attrs.update({'class': 'form-select'})
