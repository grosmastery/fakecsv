# **FakeCSV**
This project is available on PythonAnywhere:

http://grosmastery.pythonanywhere.com/

This project is designed to allow users to create a schema, generate data in a CSV file, and download that file. It was developed using Python and Django. Additionally, incorporated various data generation techniques to create a diverse range of options for users when generating data. These techniques include randomized data generation, data validation, and data normalization. Overall, this project is a powerful tool for anyone looking to create and manage their data with ease.
## **Installation**
To install this project, follow these steps:

1. Clone the repository to your local machine.
2. Install the required packages listed in `requirements.txt` by running `pip install -r requirements.txt` in your terminal.
3. Create a new virtual environment and activate it.
4. Run the `python manage.py migrate` command to create the necessary database tables.
## **Usage**
To use this project, follow these steps:
1. Create a new schema by filling out the form provided.
2. Add columns to the schema by filling out the form provided.
3. Generate data by selecting the schema and specifying the number of rows.
4. Download the CSV file containing the generated data.
