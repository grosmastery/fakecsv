from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from core.views import MainView, DownloadCSVView
from core.models import SchemaModel, TableColumnModel
from unittest.mock import patch, MagicMock
from django.urls import reverse
import os


class TestMainTestCase(TestCase):

    def setUp(self) -> None:
        self.client = Client()

    def test_diagnostic(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_get(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code == 200)

    def test_anonymous_user(self):
        response = self.client.get('/')
        self.assertEqual(response.context['all_user_schemas'], None)

    def test_user_without_schemas(self):
        get_user_model().objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        response = self.client.get('/')
        self.assertEqual(response.context['all_user_schemas'].first(), None)

    def test_user_with_schemas(self):
        user = get_user_model().objects.create_user(username='test', password='test')
        column = TableColumnModel.objects.create(
            column_name='test column',
            type_field='test type',
            integer_min='',
            integer_max='',
            order='0'
        )
        schema = SchemaModel.objects.create(
            user=user,
            schema_name='test schema name',
        )
        schema.columns.add(column)
        self.client.login(username='test', password='test')
        response = self.client.get('/')
        self.assertEqual(response.context['all_user_schemas'].first().schema_name, 'test schema name')


class SignUpTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.username = 'test'
        self.email = 'test@test.com'
        self.password = 'test'

    def test_diagnostic(self):
        response = self.client.get('/signup/')
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_get(self):
        response = self.client.get('/signup/')
        self.assertTrue(response.status_code == 200)

    def test_signup_form_success(self):
        response = self.client.post('/signup/', data={
            'username': self.username,
            'email': self.email,
            'password': self.password,
            'confirm_password': self.password,
        })
        self.assertEqual(302, response.status_code)

    def test_signup_form_decline(self):
        response = self.client.post('/signup/', data={
            'username': self.username,
            'email': self.email,
            'password': self.password,
            'confirm_password': f'{self.password}0',
        })
        self.assertEqual(200, response.status_code)


class CreateTestCase(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        self.user = get_user_model().objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')

    def test_diagnostic(self):
        response = self.client.get('/create/')
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_get(self):
        response = self.client.get('/create/')
        self.assertTrue(response.status_code == 200)

    def test_get_context_data(self):
        schema = SchemaModel.objects.create(user=self.user)
        column = TableColumnModel.objects.create(column_name='test column')
        schema.columns.add(column)

        response = self.client.get('/create/')
        self.assertEqual(str(response.context['schema_columns'].first()), 'test column')

    def test_create_success(self):
        response = self.client.post('/create/', data={
            'schema_name': '',
            'csv_delimiter': ',',
            'csv_quotechar': '"',
            'column_name': 'test',
            'data_type': 'full_name',
            'order': 1,
        })
        self.assertEqual(response.json()['status'], 'success')

    def test_create_integer_error_equal_values(self):
        response = self.client.post('/create/', data={
            'schema_name': '',
            'csv_delimiter': ',',
            'csv_quotechar': '"',
            'column_name': 'test',
            'data_type': 'integer',
            'integer_from': 1,
            'integer_to': 1,
            'order': 1,
        })
        self.assertEqual(response.json()['status'], 'integer_error')

    def test_create_integer_error_from_more_to_less(self):
        response = self.client.post('/create/', data={
            'schema_name': '',
            'csv_delimiter': ',',
            'csv_quotechar': '"',
            'column_name': 'test',
            'data_type': 'integer',
            'integer_from': 10,
            'integer_to': 1,
            'order': 1,
        })
        self.assertEqual(response.json()['status'], 'integer_error')

    def test_create_empty_integers(self):
        response = self.client.post('/create/', data={
            'schema_name': '',
            'csv_delimiter': ',',
            'csv_quotechar': '"',
            'column_name': 'test',
            'data_type': 'integer',
            'integer_from': '',
            'integer_to': '',
            'order': 1,
        })
        self.assertEqual(response.json()['status'], 'empty_fields')

    def test_create_empty(self):
        response = self.client.post('/create/', data={
            'schema_name': '',
            'csv_delimiter': ',',
            'csv_quotechar': '"',
            'column_name': '',
            'data_type': 'job',
            'integer_from': '',
            'integer_to': '',
            'order': '',
        })
        self.assertEqual(response.json()['status'], 'empty_fields')


class DeleteColumnTestCase(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        user = get_user_model().objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        column = TableColumnModel.objects.create()
        self.schema = SchemaModel.objects.create(user=user)
        self.schema.columns.add(column)

    def test_delete(self):
        self.client.post(reverse('delete_schemas_column'), {'column_id': 1})
        result = list(self.schema.columns.all())
        self.assertTrue(result == [])


class SubmitCreateSchemaTestCase(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        user = get_user_model().objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        column = TableColumnModel.objects.create()
        self.schema = SchemaModel.objects.create(user=user)
        self.schema.columns.add(column)

    def test_submit_success(self):
        response = self.client.post(reverse('submit_create_schema'), {'schema_name': 'test name', 'delimiter': ',',
                                                                      'quotechar': '"'})
        self.assertEqual(response.json()['status'], 'success')

    def test_submit_decline(self):
        response = self.client.post(reverse('submit_create_schema'), {'schema_name': '', 'delimiter': ',',
                                                                      'quotechar': '"'})
        self.assertEqual(response.json()['status'], 'decline')


class DownloadPageViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        user = get_user_model().objects.create_user(username='test', password='test')
        self.client.login(username='test', password='test')
        self.schema = SchemaModel.objects.create(user=user, schema_name='test schema', csv_delimiter=',', csv_quotechar='"')
        self.schema.columns.create(column_name='test name', type_field='full_name', order=1)
        self.schema.columns.create(column_name='test email', type_field='email', order=2)

    def tearDown(self):
        try:
            os.remove(self.schema.csv_file.path)
        except ValueError:
            pass

    def test_diagnostic(self):
        response = self.client.get(reverse('download_page', args=[self.schema.id]))
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_get(self):
        response = self.client.get(reverse('download_page', args=[self.schema.id]))
        self.assertTrue(response.status_code == 200)

    def test_get_context_data(self):
        response = self.client.get(reverse('download_page', args=[self.schema.id]))
        self.assertIn('schema', response.context)
        self.assertEqual(response.context['schema'], self.schema)
        self.assertIn('all_user_schemas', response.context)

    def test_post(self):
        self.client.post(reverse('download_page', args=[self.schema.id]), {'rows': 10})
        self.schema.refresh_from_db()
        self.assertTrue(self.schema.closed)
        self.assertTrue(os.path.exists(self.schema.csv_file.path))
        with open(self.schema.csv_file.path, 'r') as f:
            content = f.read()
        self.assertIn('test name,test email', content)


class DownloadCSVTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.file_path = 'file.csv'
        with open(self.file_path, 'w') as f:
            f.write('test content')

    def tearDown(self) -> None:
        os.remove(self.file_path)

    def test_download_csv(self):
        request = self.factory.get(reverse('download_csv', args=[self.file_path]))
        response = DownloadCSVView.as_view()(request, file_path=self.file_path)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')
        self.assertEqual(response['Content-Disposition'], f'attachment; filename={os.path.basename(self.file_path)}')
        self.assertEqual(response.content.decode(), 'test content')

