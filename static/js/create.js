// send ajax request to create new column
$(document).ready(function () {

    $('#create-btn').click(function (e) {
        e.preventDefault();
        let column_name = $('#column_name').val();
        let order = $('#order_id').val();
        let integer_from = $('#extra_input_min').val();
        let integer_to = $('#extra_input_max').val();
        let data_type = $('#id_data_type').val();
        let schema_name = $('#schema_name').val();
        let csv_delimiter = $('#id_delimiter').val();
        let csv_quotechar = $('#id_quotechar').val();
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#schema-form').attr("action");
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'column_name': column_name,
                'order': order,
                'integer_from': integer_from,
                'integer_to': integer_to,
                'data_type': data_type,
                'schema_name': schema_name,
                'csv_delimiter': csv_delimiter,
                'csv_quotechar': csv_quotechar,
                csrfmiddlewaretoken: token,
            },
            success: function (response) {
                if (response.status === 'success') {
                location.reload(true);
                // exception of empty fields "create button"
                } else if (response.status === 'empty_fields') {
                    let empty_fields = $('input[required]').filter(function() {
                    return $.trim($(this).val()) === '';
                    });
                    if (empty_fields.length) {
                        alert('Please fill in all required fields.');
                        empty_fields.css('border', '2px solid red');
                        return false;
                    }
                // exception of integer range
                } else if (response.status ==='integer_error') {
                    alert('Please fill in the correct range of integers.');
                    return false;
                }
            },
            errors: function(){
                console.log('error');
            }
            
        });
    });
});

// send ajax request to delete column
$(document).ready(function(){
    $('.delete-btn').click(function(){
        let column_id = $(this).data('column_id')
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#delete-button').attr("action");
        $.ajax({
            type: 'POST',
            url: url,
            data: {
            column_id: column_id,
            csrfmiddlewaretoken: token
            },
            success: function(){
                window.location.reload(true)
            }
            });
    });
});

// send ajax request to create new schema
$(document).ready(function() {
    $('#submit-button').click(function(){

        // exception of empty field "submit button"
        let empty_name = $('#schema_name').filter(function() {
            return $.trim($(this).val()) === '';
        });

        if (empty_name.length) {
            alert('Please fill in Name of the Schema');
            empty_name.css('border', '2px solid red');
            return false;
        }

        let schema_id = $(this).data('schema-id')
        let schema_name = $('#schema_name').val();
        let delimiter = $('#id_delimiter').val();
        let quotechar = $('#id_quotechar').val();
        let token = $('input[name=csrfmiddlewaretoken]').val();
        let url = $('#div-submit-button').attr("action");

        $.ajax({
            type: 'POST',
            url: url,
            data: {
            schema_name: schema_name,
            delimiter: delimiter,
            quotechar: quotechar,
            csrfmiddlewaretoken: token
            },
            dataType: 'json',
            success: function(response){
                if (response.status === 'success'){
                    window.location = `/download/${schema_id}/`;
                } else if (response.status === 'decline'){
                    alert('Schema must have at least 1 column');
                    return false;
                }

            }
            });
    });
});

// show/hide extra input fields
$(document).ready(function() {
    $(".from-to").css("visibility", "hidden");
    $('select[name="data_type"]').change(function() {
        let selected = $(this).val();
        if (selected == 'integer') {
            $('.from-to').css("visibility", "visible");
            $('.from-to').prop('required', true);
        } else {
            $('.from-to').css("visibility", "hidden");
            $('.from-to').prop('required', false);
        }
    });
});

// show/hide extra column fields
$(document).ready(function() {
    $('.schema-table tbody tr').each(function() {
        if ($(this).find('.column-from-to').filter(function() { return !!this.value; }).length === 0) {
            $(this).find('.column-from-to').css("visibility", "hidden");
        } else {
            $(this).find('.column-from-to').css("visibility", "visible");
        }
    });
});


