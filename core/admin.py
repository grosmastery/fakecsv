from django.contrib import admin
from core.models import TableColumnModel, SchemaModel


@admin.register(TableColumnModel)
class TableColumnAdmin(admin.ModelAdmin):
    pass


@admin.register(SchemaModel)
class SchemaAdmin(admin.ModelAdmin):
    pass

