import csv
import os
import random

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, View, FormView
from faker import Faker

from core.forms import CreateSchemaForm, SignUpForm
from core.models import TableColumnModel, SchemaModel

fake = Faker()


class MainView(TemplateView):
    template_name = 'main.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['all_user_schemas'] = SchemaModel.objects.filter(user=self.request.user)
        else:
            context['all_user_schemas'] = None
        return context


class SignUpView(FormView):
    template_name = 'sign-up.html'
    form_class = SignUpForm
    success_url = '/login/'

    def form_valid(self, form):
        form.save()
        return super(SignUpView, self).form_valid(form)


class CreateView(LoginRequiredMixin, TemplateView):
    template_name = 'create.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        schema = SchemaModel.objects.filter(user=self.request.user, closed=False).first()
        context = {
            'form': CreateSchemaForm(initial={'delimiter': schema.csv_delimiter if schema else ',',
                                              'quotechar': schema.csv_quotechar if schema else '"'}),
            'schema': schema,
            'schema_columns': schema.columns.all() if schema is not None else None,
        }
        return context

    def post(self, request):
        item = request.POST.copy()
        if item['data_type'] == 'integer':
            if item['integer_from'] and item['integer_to']:
                if item['integer_from'] == item['integer_to'] or item['integer_from'] > item['integer_to']:
                    return JsonResponse({'status': 'integer_error'})
            empty_fields = ['column_name', 'order', 'integer_from', 'integer_to']
        else:
            item['integer_from'] = ''
            item['integer_to'] = ''
            empty_fields = ['column_name', 'order']
        if any(item.get(field) == '' for field in empty_fields):
            return JsonResponse({'status': 'empty_fields'})

        table_column = TableColumnModel.objects.create(column_name=item['column_name'],
                                                       type_field=item['data_type'],
                                                       integer_min=item['integer_from'],
                                                       integer_max=item['integer_to'],
                                                       order=item['order'])
        get_schema = SchemaModel.objects.filter(user=request.user, closed=False).first()
        if get_schema:
            get_schema.columns.add(table_column)
            SchemaModel.objects.filter(user=request.user, closed=False).update(schema_name=item['schema_name'],
                                                                               csv_delimiter=item['csv_delimiter'],
                                                                               csv_quotechar=item['csv_quotechar'])
        else:
            get_schema = SchemaModel.objects.create(user=request.user, schema_name=item['schema_name'],
                                                    csv_delimiter=item['csv_delimiter'],
                                                    csv_quotechar=item['csv_quotechar'])
            get_schema.columns.add(table_column)
        return JsonResponse({'status': 'success'})


class DeleteColumnView(View):

    def post(self, request):
        item = get_object_or_404(TableColumnModel, id=request.POST['column_id'])
        item.delete()
        return JsonResponse({'status': 'success'})


class SubmitCreateSchemaView(View):

    def post(self, request):
        schema_data = request.POST
        schema = SchemaModel.objects.filter(user=request.user, closed=False)
        if schema.exists():
            schema.update(schema_name=schema_data['schema_name'], csv_delimiter=schema_data['delimiter'],
                          csv_quotechar=schema_data['quotechar'])
            schema = schema.first()
        else:
            schema = SchemaModel.objects.create(user=request.user,
                                                schema_name=schema_data['schema_name'],
                                                csv_delimiter=schema_data['delimiter'],
                                                csv_quotechar=schema_data['quotechar'])
        if schema.columns.exists():
            if schema.schema_name:
                return JsonResponse({'status': 'success'})
        return JsonResponse({'status': 'decline'})


class DownloadPageView(LoginRequiredMixin, TemplateView):
    template_name = 'download.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['schema'] = get_object_or_404(SchemaModel, id=kwargs['id'])
        context['all_user_schemas'] = SchemaModel.objects.filter(user=self.request.user)
        return context

    def post(self, request, id):
        rows = request.POST['rows']
        schema = get_object_or_404(SchemaModel, id=id)

        media_folder = os.path.join(settings.MEDIA_ROOT, '')
        if not os.path.exists(media_folder):
            os.makedirs(media_folder)

        file_path = os.path.join(media_folder, f'{schema.schema_name}-{schema.id}.csv')
        with open(file_path, 'w', encoding='utf-8') as f:
            writer = csv.writer(f, delimiter=schema.csv_delimiter, quotechar=schema.csv_quotechar)

            headers = [column.column_name for column in schema.columns.all().order_by('order')]
            writer.writerow(headers)
            for i in range(int(rows)):
                row = []
                for column in schema.columns.all().order_by('order'):
                    if column.type_field == 'full_name':
                        row.append(fake.name())
                    elif column.type_field == 'job':
                        row.append(fake.job())
                    elif column.type_field == 'email':
                        row.append(fake.email())
                    elif column.type_field == 'domain_name':
                        row.append(fake.domain_name())
                    elif column.type_field == 'phone_number':
                        row.append(fake.phone_number())
                    elif column.type_field == 'company_name':
                        row.append(fake.company())
                    elif column.type_field == 'text':
                        row.append(fake.text())
                    elif column.type_field == 'integer':
                        row.append(random.randrange(int(column.integer_min), int(column.integer_max)))
                    elif column.type_field == 'address':
                        row.append(fake.address())
                writer.writerow(row)
            schema.csv_file = file_path
            schema.closed = True
            schema.save()
        return JsonResponse({'status': 'success'})


class DownloadCSVView(View):

    def get(self, request, file_path, *args, **kwargs):
        # without this line pythonanywhere will not find the file
        # file_path = f'/{file_path}'
        with open(file_path, 'rb') as f:
            response = HttpResponse(f.read(), content_type='text/csv')
            response['Content-Disposition'] = f'attachment; filename={os.path.basename(file_path)}'
            return response
