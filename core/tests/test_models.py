from django.test import TestCase
from django.contrib.auth import get_user_model
from core.models import SchemaModel, TableColumnModel


class SchemaModelTestCase(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='test', email='test@example.com', password='test'
        )
        self.schema = SchemaModel.objects.create(
            user=self.user,
            schema_name='test schema',
            closed=False,
            csv_file=None,
            csv_delimiter=',',
            csv_quotechar='"'
        )
        self.column = TableColumnModel.objects.create(
            column_name='test-column',
            type_field='test-type',
            integer_min=None,
            integer_max=None,
            order='1'
        )

    def test_schema_and_column_creation(self):
        self.assertEqual(SchemaModel.objects.count(), 1)
        self.assertEqual(TableColumnModel.objects.count(), 1)

    def test_schema_string_representation(self):
        self.assertEqual(str(self.schema), 'test schema')

    def test_column_string_representation(self):
        self.assertEqual(str(self.column), 'test-column')

    def test_adding_column_to_schema(self):
        self.schema.columns.add(self.column)
        self.assertTrue(self.schema.columns.filter(pk=self.column.pk).exists())

    def test_schema_is_not_closed_by_default(self):
        self.assertFalse(self.schema.closed)

    def test_schema_has_user(self):
        self.assertEqual(self.schema.user, self.user)
