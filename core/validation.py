from re import compile as recompile

from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class PinValidator:
    pin_regex = recompile('\w{4,}')

    def validate(self, password, user=None):
        if not self.pin_regex.fullmatch(password):
            raise ValidationError(_('The password must contain at least 4 characters.'), code='pin_4_chars')

    def get_help_text(self):
        return _('Your password must contain at least 4 characters.')
